# ics-ans-role-juniper-name-server

Ansible role to configure switch to use these DNS servers.

## Role Variables

```yaml
juniper_name_server_dns: []
  # - 172.16.2.21
  # - 172.16.2.22
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-name-server
```

## License

BSD 2-clause
