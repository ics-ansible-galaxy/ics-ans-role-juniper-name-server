import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_name_servers_added(host):
    assert os.system("zcat /config/juniper.conf.gz | grep name-server")
